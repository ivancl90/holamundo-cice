package com.example.holamundo

import android.util.Log

interface SecondaryRecyclerActions {

    fun onItemSelected(monthName: String) {
        Log.d("CICE", monthName)
    }
}