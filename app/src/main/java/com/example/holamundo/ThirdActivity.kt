package com.example.holamundo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_third.*

class ThirdActivity : AppCompatActivity() {


    //region LYFE CYCLE
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)
        definePager()
    }
    //endregion LYFE CYCLE


    //region UI
    private fun definePager(){
        val months = resources.obtainTypedArray(R.array.secondaryItems)

        viewPager.adapter = ThirdPagerAdapter(supportFragmentManager, months)

    }
    //endregion UI

}