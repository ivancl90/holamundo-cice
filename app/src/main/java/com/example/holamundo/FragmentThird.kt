package com.example.holamundo

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_third.view.*

class FragmentThird : Fragment() {

    companion object {
        const val MONTH = "month"

        fun newInstance(text: String): FragmentThird {
            val arguments = Bundle()
            arguments.putString(MONTH, text)
            val fragment = FragmentThird()
            fragment.arguments = arguments
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_third, container, false)
        view.textMonth.text = arguments?.getString(MONTH) ?: "UN MES CUALQUIERA"
        Log.d("CICE","onCreateView")
        return view
    }

    override fun onResume() {
        super.onResume()
        this.isVisible
        Log.d("CICE","onResume")
    }


}