package com.example.holamundo

import android.os.Bundle
import android.support.asynclayoutinflater.R.id.async
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.holamundo.models.Categories
import com.example.holamundo.models.Joke
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_third_grid.view.*
import java.net.URL

class FragmentThirdGrid : Fragment() {
    lateinit var recycler: RecyclerView
    lateinit var buttonJokes: Button

    companion object {
        fun newInstance(): FragmentThirdGrid {
            return FragmentThirdGrid()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_third_grid, container, false)
        initUi(view)
        return view
    }


    private fun initUi(rootView: View) {
        //RECYCLE
        recycler = rootView.recyclerGrid
        recycler.layoutManager = GridLayoutManager(context, 2)
        if (context != null)
            rootView.recyclerGrid.adapter = FragmentThidGridAdapter(context!!)

        //BUTTON
        buttonJokes = rootView.buttonJokes
        buttonJokes.setOnClickListener {
            getJoke()
        }
    }

    private fun getJoke() {
        var result: String
        Thread {
            result = URL("https://api.chucknorris.io/jokes/categories").readText()
            activity?.runOnUiThread {
                Log.d("CICE", result)

                var joke = Gson().fromJson(result, Categories::class.java)

                Log.d("CICE", joke.size.toString())

            }
        }.start()
    }
}