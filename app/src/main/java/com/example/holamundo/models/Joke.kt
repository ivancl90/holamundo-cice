package com.example.holamundo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Joke(var category : String?,
                var icon_url : String?,
                var id : String?,
                var url : String?,
                var value : String? ) : Parcelable