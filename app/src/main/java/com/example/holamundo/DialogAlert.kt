package com.example.holamundo

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.example.holamundo.models.Dialog
import kotlinx.android.synthetic.main.dialog_fragment.*
import kotlinx.android.synthetic.main.dialog_fragment.view.*


class DialogAlert : AppCompatDialogFragment() {
    companion object {
        const val DIALOG = "dialog"

        fun newInstance(dialog : Dialog): DialogAlert {

            val dialogAlert = DialogAlert()
            val arguments = Bundle()

            arguments.putParcelable(DIALOG, dialog)

            dialogAlert.arguments = arguments

            return dialogAlert
        }
    }

    lateinit var listener : DialogAction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(AppCompatDialogFragment.STYLE_NORMAL, R.style.DialogTheme)
        isCancelable = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.dialog_fragment, container, false)

        var dialog : Dialog = arguments?.getParcelable(DIALOG) ?: Dialog()

        view.textTitle.text = dialog.title ?: getString(R.string.app_name)
        view.textMessage.text = dialog.text

        view.buttonPositive.text = dialog.buttonPositive
        if (dialog.buttonPositive != null) {
        } else
            view.buttonPositive.visibility = GONE

        if (dialog.buttonNegative != null) {
            view.buttonNegative.text = dialog.buttonNegative
        } else
            view.buttonNegative.visibility = GONE

        view.buttonPositive.setOnClickListener {
            this.dismiss()
            listener.positiviClick(0)
        }

        view.buttonNegative.setOnClickListener {
            this.dismiss()
            listener.negativeClick(0)
        }

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (parentFragment != null && parentFragment is DialogAction)
            listener = parentFragment as DialogAction
        else if (context is DialogAction)
            listener = context
        else if (childFragmentManager is DialogAction)
            listener = childFragmentManager as DialogAction
        else
            RuntimeException("Error listener global action isnt implemented")
    }


}